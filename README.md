# Keyoxide Checkify

This browser addon displays a check mark next to Mastodon profile names in the home timeline if they're verified via Keyoxide. It's a work-in-progress, so don't expect it to work. The major problem is that the addon doesn't notice when the user switches from a different tab (e.g. the notifications) back to the home timeline.

## Try it out

1. Download this Git repo.
2. Open `about:debugging#/runtime/this-firefox` in Firefox.
3. Click on “Load temporary addon” and select `manifest.json`.

## Licence

Keyoxide Checkify uses [doip.js](https://codeberg.org/keyoxide/doipjs/src/branch/main/dist/doip.min.js) which is licensed under the [Apache 2.0 Licence](https://codeberg.org/keyoxide/doipjs/src/branch/main/LICENSE) and which was created by [Yarmo Mackenbach](https://codeberg.org/yarmo).

Keyoxide Checkify itself is licensed under the [MIT Licence](LICENCE.md) (slightly modified).